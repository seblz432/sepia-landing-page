import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/waitlist.module.css'

export default function About() {

  return (
    <div className={styles.container} id="waitlist">
      <h2 className={styles.header2}>We're not quite ready yet!</h2>

      <p className={styles.bodyText}>We're still hard at work getting the service ready. Enter your email below if you'd like early access or just to be kept up to date!</p>

      <div className={styles.separator}/>

      <div className={styles.waitlistContainer}>
        {/*<input className={styles.input} type="email" name="email" placeholder="john.doe@example.com"/>
        <button className={styles.button}>Join the waitlist</button>*/}

        <div id="mc_embed_signup">
          <form action="https://sepia.us17.list-manage.com/subscribe/post?u=1e17d621b2dfbe6b4fe07cb9a&amp;id=d27f346a3b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll" className={styles.waitlistContainer}>
              <div class="mc-field-group">
          	   <input required className={"waitlist-input"} placeholder="john.doe@example.com" type="email" name="EMAIL" class="required email" id="mce-EMAIL"/>
              </div>

            	<div id="mce-responses" class="clear">
            		<div class="response" id="mce-error-response" style={{display: 'none'}}></div>
            		<div class="response" id="mce-success-response" style={{display: 'none'}}></div>
            	</div>   {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}

              <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true"><input type="text" name="b_1e17d621b2dfbe6b4fe07cb9a_d27f346a3b" tabindex="-1" value=""/></div>
              <div class="clear">
                <input className={"waitlist-button"} type="submit" value="Join the waitlist" name="subscribe" id="mc-embedded-subscribe" class="button"/>
              </div>
            </div>
          </form>
        </div>
      </div>

      <p className={styles.smallerBodyText}>(We promise not to spam you or give away your email)</p>
    </div>

  )
}
