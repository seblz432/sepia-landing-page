import Head from 'next/head'
import { useState } from 'react'

import Navigation from '../components/navigation'

import Home from '../components/home'
import About from '../components/about'
import Pricing from '../components/pricing'
import Contact from '../components/contact'
import Footer from '../components/footer'

export default function Sepia() {

  const [currentPage, setCurrentPage] = useState("home");

  const handleVisible = (page) => {
    setCurrentPage(page);
  }

  return (
    <>
      <Head>
        <title>Sepia</title>
        <link rel="icon" href="/favicon.ico?v=2" />
        <link rel="manifest" href="/manifest.json" />
        <meta
          name="description"
          content="The ethical way to privately backup your photos and videos. Open source and user-friendly using renewable energy."
        />
      </Head>

      <Navigation page={currentPage} />

      <Home handleVisible={handleVisible} />

      <About handleVisible={handleVisible} />

      <Pricing handleVisible={handleVisible} />

      <Contact handleVisible={handleVisible} />

      <Footer />

    </>
  )
}
