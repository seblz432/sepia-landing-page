import styles from '../styles/waitlist.module.css'

export default function About() {

  return (
    <div className={styles.container} id="privacy">
      <h1>Privacy Policy</h1>

      <p>We do not individually track users or collect any personally identifiable information.</p>

      <p>The only information we collect on our users is their email, processed through Mailchimp, along with anonymous analytics processed through Plausible.io</p>

      <h2>Consent</h2>

      <p>By using our website, you thereby consent to our Privacy Policy.</p>
    </div>
  )
}
