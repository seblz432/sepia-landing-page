import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/contact.module.css'
import VisibilitySensor from 'react-visibility-sensor';

export default function Contact(props) {
  const router = useRouter()

  function onChange (isVisible) {
    if (isVisible) {
      props.handleVisible("contact")
    }
  }

  return (
    <div className={styles.container} id="contact">
        <h2 className={styles.header2}>Contact Us</h2>

        <span className={styles.bodyText}>General support questions? Contact <a className={styles.email} href="mailto:support@sepia.co">support@sepia.co</a></span>

        <VisibilitySensor onChange={onChange}>
          <div className={styles.contactsContainer}>
            <div className={styles.contactPerson}>
              <img className={styles.headshot} src="./assets/sebastianHeadshot.jpg" />
              <span className={styles.personName}>Sebastian Gale</span>
              <a className={styles.personEmail} href="mailto:sebastian@sepia.co">sebastian@sepia.co</a>
              <p className={styles.bio}>Hi, I'm building Sepia because people deserve to have a choice that is ethical and supports their privacy. I have 7+ years of experience in both software development and business so I'm going to build a better service, because someone should.</p>
            </div>

            {/*<div className={styles.contactPerson}>
              <div className={styles.headshot} />
              <span className={styles.personName}>Lorem Ipsum | CMO</span>
              <a className={styles.personEmail} href="mailto:lorem@sepia.co">lorem@sepia.co</a>
              <p className={styles.bio}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>*/}
          </div>
        </VisibilitySensor>

        <span style={{color: 'grey'}}>*Number of photos is a conversative estimate calculated using an average of 3MB per photo</span>
    </div>

  )
}
