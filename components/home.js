import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/home.module.css'
import VisibilitySensor from 'react-visibility-sensor';

import Divider from '../public/assets/divider.svg';

import LaptopLogo from '../public/assets/laptop-outline.svg'
import AndroidLogo from '../public/assets/logo-android.svg'
import IOSLogo from '../public/assets/logo-apple.svg'


export default function Home(props) {
  const router = useRouter()

  function onChange (isVisible) {
    if (isVisible) {
      props.handleVisible("home");
    }
  }

  return (
    <VisibilitySensor onChange={onChange}>
      <div className={styles.container} id="home">

        <div className={styles.bottomDiagonal}>
          <Divider className={styles.divider} />
          <div className={styles.dividerBackground} />
        </div>

        <div className={styles.descriptionContainer}>
          <span>
            <span className={styles.descriptionFirst}>Backup your photos and videos</span>
            <br/><br/>
            <span className={styles.descriptionSecond}>Open source and privacy-friendly</span>
            {/*<br/><br/>
            <div className={styles.logosContainer}>
              <LaptopLogo style={{marginTop: '5px', marginRight: '5px'}}/>
              <AndroidLogo />
              <IOSLogo />
            </div>*/}
          </span>
        </div>

        <div className={styles.main}>
          <h1 className={styles.title}>
              Sepia
          </h1>

          {/*<p className={styles.tagline}>
            Your photos matter, so does your privacy
          </p>*/}
        </div>
      </div>
    </VisibilitySensor>
  )
}
