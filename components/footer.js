import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/footer.module.css'

export default function About(props) {

  const router = useRouter()

  return (
    <div className={styles.container} id="footer">
        <a className={styles.item} href="https://gitlab.com/seblz432/sepia-landing-page" target="_blank" rel="noopener noreferrer">Website source code</a>
        <a className={styles.item} href="https://gitlab.com/seblz432/sepia-frontend" target="_blank" rel="noopener noreferrer">App source code</a>
        <a className={styles.item} onClick={()=>{router.push("/privacy")}}>Privacy Policy</a>
    </div>
  )
}
