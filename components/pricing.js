import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/pricing.module.css'
import VisibilitySensor from 'react-visibility-sensor';

export default function About(props) {
  const router = useRouter()

  function onChange (isVisible) {
    if (isVisible) {
      props.handleVisible("pricing")
    }
  }

  return (
    <div className={styles.container} id="pricing">

      {/*<img src="/assets/pricingImage1.jpg" style={{position: 'absolute', width: '35%', minWidth: '250px', top: '15%', left: '-30px', borderRadius: '10px'}}/>

      <img src="/assets/pricingImage2.jpg" style={{position: 'absolute', height: '25%', minHeight: '250px', top: '5%', left: '50%', borderRadius: '10px'}}/>

      <img src="/assets/pricingImage3.jpg" style={{position: 'absolute', height: '25%', minHeight: '250px', bottom: '5%', right: '5%', borderRadius: '10px'}}/>

      <img src="/assets/pricingImage4.jpg" style={{position: 'absolute', width: '12.5%', minWidth: '50px', bottom: '2%', left: '30%', borderRadius: '10px'}}/>*/}

      {/*<h2 className={styles.title}>Pricing</h2>*/}

        <div className={styles.pricingContainer}>

          <div className={styles.pricingCard}>
            <h2 className={styles.planTitle}>Basic</h2>
            <h2 className={styles.planPrice}>$3.29 CAD <span className={styles.planPeriod}>/month</span></h2>
            <span className={styles.feature}>Store 100GB/30,000* photos</span>
            <span className={styles.feature}>Suited to store your many selfies</span>
            <button className={styles.planButton} onClick={()=>{router.push("/waitlist")}}>Get 7 days free</button>
          </div>

          <VisibilitySensor onChange={onChange}>
            <div className={styles.pricingCard} style={{transform: 'scale(1.2)'}}>
              <h2 className={styles.planTitle} style={{color: '#be884b'}}>Standard</h2>
              <h2 className={styles.planPrice}>$4.39 CAD <span className={styles.planPeriod}>/month</span></h2>
              <span className={styles.feature}>Store 200GB/65,000* photos</span>
              <span className={styles.feature}>Safeguard your life's memories</span>
              <button className={styles.planButton} style={{backgroundColor: '#be884b'}} onClick={()=>{router.push("/waitlist")}}>Get 7 days free</button>
            </div>
          </VisibilitySensor>

          <div className={styles.pricingCard}>
            <h2 className={styles.planTitle}>Advanced</h2>
            <h2 className={styles.planPrice}>$10.29 CAD <span className={styles.planPeriod}>/month</span></h2>

            <span className={styles.feature}>Store 500GB/165,000 photos</span>
            <span className={styles.feature}>Perfect for many  high quality images and videos</span>
            <button className={styles.planButton} onClick={()=>{router.push("/waitlist")}}>Get 7 days free</button>
          </div>

        </div>
    </div>
  )
}
