import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/about.module.css'
import VisibilitySensor from 'react-visibility-sensor';

import DiversityIllus from '../public/assets/diversity.svg'
import SecurityIllus from '../public/assets/security.svg'
import DesignIllus from '../public/assets/design.svg'

export default function About(props) {
  const router = useRouter()

  function onChange (isVisible) {
    if (isVisible) {
      props.handleVisible("about")
    }
  }

  return (
    <div className={styles.container} id="about">
        <img className={styles.mockup} src="/assets/apple-multi.png"/>

        <VisibilitySensor onChange={onChange}>
          <h2 className={styles.header2}>Why Sepia?</h2>
        </VisibilitySensor>

        <div className={styles.pointsContainer}>
          <div className={styles.point}>
            <DiversityIllus className={styles.illustration} style={{paddingTop: '25px'}}/>

            <h3 className={styles.header3}>Privacy, for everyone</h3>

            <p className={styles.bodyText}>
              Privacy shouldn't only be available to technical people with a ton of spare time who are willing to compromise on convenience and functionality.
            </p>
          </div>

          <div className={styles.point}>
            <SecurityIllus className={styles.illustration}/>

            <h3 className={styles.header3}>Secure & open source</h3>

            <p className={styles.bodyText}>
              Everything you store with us is securely stored in multiple encrypted copies across two data centres. We have taken great care to ensure the security of our system and the best part is, you don't have to take our word for it because everything we do is <a className={styles.link} href="https://gitlab.com/seblz432/sepia-landing-page" target="_blank" rel="noopener noreferrer">open-source</a>.
            </p>
          </div>

          <div className={styles.point}>
            <DesignIllus className={styles.illustration}/>

            <h3 className={styles.header3}>User-friendly</h3>

            <p className={styles.bodyText}>
              We're designing a grandma friendly app for all platforms, because privacy shouldn't sacrifice usability. It auto-uploads your photos and videos, keeping it easy to view, download, and share.
            </p>
          </div>
        </div>

        {/*<VisibilitySensor onChange={onChange}>
          <p className={styles.bodyText}>With everything we do, we believe in doing the right thing for our users according to our values. In this age of rapidly evolving online expansion, the digital world has never had so much influence over our lives. Unfortunately, people often have to choose between privacy and security or benefiting from all the advantages that come with many online services. Sepia is our way of giving people a real choice for their freedom.</p>
        </VisibilitySensor>

        <h2 className={styles.header2}>How it works</h2>

        <p className={styles.bodyText}>Our grandma friendly app will automatically upload your photos and videos to our high security servers. Based out of Canada and storing your media using 100% clean energy datacenters in Europe, your photos are safeguarded by strong privacy laws. Within the app you can easily view and share your photos and videos, ensuring that you always have easy access.</p>*/}
    </div>

  )
}
