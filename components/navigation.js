import styles from '../styles/navigation.module.css'
import { useRouter } from 'next/router'
import { useState } from 'react'
import Link from 'next/link'

const MenuIcon = (props) => {
  return (
    <svg className={"ham hamRotate ham4 " + props.activeClass} viewBox="0 0 100 100" width="80">
      <path
            className="line top"
            d="m 70,33 h -40 c 0,0 -8.5,-0.149796 -8.5,8.5 0,8.649796 8.5,8.5 8.5,8.5 h 20 v -20" />
      <path
            className="line middle"
            d="m 70,50 h -40" />
      <path
            className="line bottom"
            d="m 30,67 h 40 c 0,0 8.5,0.149796 8.5,-8.5 0,-8.649796 -8.5,-8.5 -8.5,-8.5 h -20 v 20" />
    </svg>
  )
}

export default function Navigation (props) {
  const router = useRouter()

  const [menu, setMenu] = useState(false);

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={ menu ? styles.activeMobileMenu : styles.mobileMenu }>
          <a onClick={() => {setMenu(false); document.getElementById("home").scrollIntoView()}} className={styles.link}>Home</a>
          <a onClick={() => {setMenu(false); document.getElementById("about").scrollIntoView()}} className={styles.link}>About</a>
          <a onClick={() => {setMenu(false); document.getElementById("pricing").scrollIntoView()}} className={styles.link}>Pricing</a>
          <a onClick={() => {setMenu(false); document.getElementById("contact").scrollIntoView()}} className={styles.link}>Contact</a>
        </div>

        <a className={styles.menuButton} onClick={() => {setMenu(!menu)}}>
          <MenuIcon activeClass={ menu ? 'active' : '' }/>
        </a>

        <div className={styles.links}>
          <a onClick={()=>{document.getElementById("home").scrollIntoView()}} className={styles.link} id={ (props.page === "home") ? "activeLink" : ""}>Home</a>
          <a onClick={()=>{document.getElementById("about").scrollIntoView()}} className={styles.link} id={ (props.page === "about") ? "activeLink" : ""}>About</a>
          <a onClick={()=>{document.getElementById("pricing").scrollIntoView()}} className={styles.link} id={ (props.page === "pricing") ? "activeLink" : ""}>Pricing</a>
          <a onClick={()=>{document.getElementById("contact").scrollIntoView()}} className={styles.link} id={ (props.page === "contact") ? "activeLink" : ""}>Contact</a>
        </div>

        <button className={styles.link} onClick={()=>{router.push("/waitlist")}}>Sign up/Login</button>
      </div>
    </div>
  )
}
