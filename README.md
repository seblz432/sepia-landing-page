Looking for the [source code for our apps](https://gitlab.com/seblz432/sepia-frontend)?

## Welcome to the source code for Sepia's landing page located at sepia.co!

In an effort to be as transparent as possible, we have released the source code for our website.

**Please note** this source code is provided without a licence and as such the author retains full copyright to the work made available here.

Feel free to to go over it, review it, learn from it, however you may **not** copy this code for anything other than personal use, as we don't want people stealing our company website.


## Below you will find instructions and resources

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

### Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
